import numpy as np
import matplotlib.pyplot as plt
import net
from torch import Tensor
import torch
from torch.utils.data import DataLoader, TensorDataset

with np.load('prediction-challenge-02-data.npz') as fh:
    x_train = fh['x_train']
    y_train = fh['y_train']
    x_test = fh['x_test']

#net = net.Net()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
#net.to(device)

print(x_train.shape)

idx = np.arange(len(x_train))
#np.random.shuffle(idx)

q = int(0.8*x_train.shape[0])
train_idx = idx[:q]
valid_idx = idx[q:]

#Trafo train set in Tensor
train_x = Tensor(train_x).to(device)
train_y = Tensor(train_y).long().to(device)
train = TensorDataset(train_x, train_y)

#Trafo valid set in Tensor
valid_x = Tensor(valid_x).to(device)
valid_y = Tensor(valid_y).to(device)
valid = TensorDataset(valid_x, valid_y)

#Trafo tets set in Tensor
test_x = Tensor(x_test).to(device)

#Shuffle and Batch size it
data = DataLoader(train, batch_size=100,shuffle=True)
valid = DataLoader(valid, shuffle=True)

# TRAINING DATA: INPUT (x) AND OUTPUT (y)
# 1. INDEX: IMAGE SERIAL NUMBER (6000)
# 2/3. INDEX: PIXEL VALUE (32 x 32)
# 4. INDEX: COLOR CHANNELS (3)
#print(x_train.shape, x_train.dtype)
#print(y_train.shape, y_train.dtype)

# TEST DATA: INPUT (x) ONLY
#print(x_test.shape, x_test.dtype)

# TRAIN MODEL ON x_train, y_train

# PREDICT prediction FROM x_test

# MAKE SURE THAT YOU HAVE THE RIGHT FORMAT
#assert prediction.ndim == 1
#assert prediction.shape[0] == 300

# AND SAVE EXACTLY AS SHOWN BELOW
#np.save('prediction.npy', prediction)