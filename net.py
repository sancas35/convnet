import torch
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super().__init__()

        #Start with two convnets
        # not sure of kernel size, output size etc yet, just someplaceholders for now
        self.features = nn.Sequential(
            nn.Conv2d(1,32,5,1),
            nn.ReLU(),
            nn.AvgPool2d(3),
            nn.Conv2d(32,64,3,1),
            nn.BatchNorm2d()
            
        )
        self.classifier = nn.Sequential(
            self.fc1 = nn.Linear(..., ...),
            self.fc2 = nn.Linear(..., 3)                        # 3 possible outputs dog,cat,frog
        )

        self.drop_out = nn.Dropout()
        
    def forward(self, x):
        x = self.features(x)
        # Use dropout method ??
        x = self.drop_out(x)
        x = self.classifier(x)

        return F.log_softmax(x,1)
